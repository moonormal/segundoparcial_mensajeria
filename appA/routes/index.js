var express = require('express');
var router = express.Router();

const controller= require('../controllers/AppA');
/* GET home page. */

router.get('/', controller.home);

router.post('/recibir', controller.recibir);

router.post('/mandar', controller.mandar);

module.exports = router;
