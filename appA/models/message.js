const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _message: String
});

class Message {

    constructor(message) {
        this._message = message;
    }
    
    get message() {
        return this._message;
    }
    set message(v) {
        this._message = v;
    }

}

schema.loadClass(Message);
module.exports = mongoose.model('Message', schema);
