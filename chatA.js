const amqp = require('amqplib/callback_api');

// amqp://<user>?:?<password>?@?<host:port>
amqp.connect("amqps://MelissaMarina:contrasena1234@b-646e8969-8354-4d4c-bd23-c9e6f9e86b32.mq.us-east-1.amazonaws.com:5671",(err,con)=>{

  if(err){
    throw err;
  }

  con.createChannel((err1, channel)=>{
    if(err1){
      throw err1;
    }

    let queueAB="mensajesAB";
    let queueBA="mensajesBA"
    let stdin = process.openStdin();

    channel.assertQueue(queueBA, {
      durable: false
    });

    channel.consume(queueBA, (message)=>{
      console.log("-> "+ message.content.toString());
    }, {noAck: true});

    stdin.addListener("data", function(message) {

      channel.assertQueue(queueAB, {
        durable: false
      });

      console.log("");
      channel.sendToQueue(queueAB, Buffer.from(message));


    });

  });

});

