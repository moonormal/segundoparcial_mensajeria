# Proyecto del segundo parcial

Para el segundo parcial vamos a implementar una arquitectura de alta disponibilidad basada en mensajería, el siguiente diagrama ilustra cómo debería funcionar la arquitectura en formato general. Para completar el proyecto de forma satisfactoria deberá:

a) Completar la arquitectura y su diagrama investigando cuál es el componente faltante (25 puntos).

b) Crear las aplicaciones cliente y servidor y entregar las ligas de git. (25 puntos)

c) Implementar la arquitectura de forma funcional, documentando su creación en una presentación paso a paso (50 puntos).

![Image text](Arquitectura.png)

## Autores

Ana Marina Ortiz 329575

Melissa García Mendoza 334259

## Grupo 🚀
Cloud Computing 8CC2
